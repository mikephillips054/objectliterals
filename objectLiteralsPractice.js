let car1 = { year: "2017", make: "Volkswagen", model: "Golf GTI", colour: "Alpine White", kms: "1,400km", registration: "BGD461", price: "$82,000"}//Method 1

let car2 = {
    year:         "2018" ,
    make:         "Nissan" ,
    model:        "GTR Nismo Edition",
    colour:       "Midnight Blue",
    kms:          "2,000km",
    registration: "LKC859",
    price:        "$185,000" ,
   
}//Method 2

let car3 = new Object()//Method 3
car3.Year =     "2016"
car3.Make =     "Audi"
car3.Model =    "R8 Quattro V10"
car3.Colour =   "Racing Yellow"
car3.KMs =      "900km"
car3.Registration = "LC129"
car3.Price =    "$287,000"

/*---Outputs---*/
let string1 = "---Car 1--- "
for(let x in car1)
{
    string1 += car1[x] + " ";
 
}
//document.getElementById("output1").innerhtml = string1 + "<br><br>"; //1
console.log(string1)


let string2 = "---Car 2--- "
for(let x in car2)
{
    string2 += car2[x] + " ";
}
//document.getElementById("output2").innerhtml = string2 + "<br><br>";//2
console.log(string2)

let string3 = "---Car 3--- "
for(let x in car3)
{
    string3 += car3[x] + " ";
}
//document.getElementById("output3").innerhtml = string3 + "<br><br>";//3
console.log(string3)

/*---Constructor Functions---*///Method 4
let vehicle = function()
{
    this.year;
    this.make;
    this.model;
}
let car4 = new vehicle();
car4.year = " 2017"
car4.make = "Lamborghini"
car4.model = "Aventador"
car4.colour =   "Onyx Black"
car4.kms =      "1,000km"
car4.registration = "JK259"
car4.price = "$320,000"

//document.getElementById("output4").innerhtml = string4 + "<br><br>";//4
let string4 = "---Car 4---"
for(let x in car4)
{
    string4 += car4[x] + " ";
}
//document.getElementById("output4").innerhtml = string4 + "<br><br>"
console.log(string4)

//Delete the "colour" property
delete car1["colour"]                             //delete the "colour" property from car1
delete car2["colour"]                             //delete the "colour" property from car2



//Display all properties of object "car1"
console.log("\nProperties of 'car1' - with the deleted property (colour):")         //display heading
console.log(string1)
console.log("\nProperties of 'car2' - with the deleted property (colour):")         //display heading
console.log(string2)



console.log("")